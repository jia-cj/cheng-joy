console.log("Hello, B297!");

//Functions
	//Parameters and Arguments

/*
 function printName(){

 	let nickName = prompt("Enter your Nickname")
 	console.log("Hi, " + nickname);

 }

 	printName();*/

	function printName(name){
 		console.log("Hi, " + name);

} 

 	printName("JC");

 	let sampleVariable = "Cardo";

 	printName(sampleVariable);

 	function checkDivisibilityBy8(num){

 		let remainder = num % 8;
 		console.log("The remainder of" + num + "divided by 8 is:" + remainder);
 		let isDivisibileBy8 = remainder === 0;
 		console.log(isDivisibileBy8);

 	}

 	checkDivisibilityBy8(64);
 	checkDivisibilityBy8(28);
 	checkDivisibilityBy8(9678);

 /*
 	Mini-Activity (5min.)
 	check the divisibilty of a number by 4 have one parameter named num

 	1. 56
 	2. 95
 	3. 444

*/
		function checkDivisibilityBy4(num) {
		let remainder = num % 4;
 		console.log("The remainder of" + num + "divided by 4 is:" + remainder);
 		let isDivisibileBy4 = remainder === 0;
 		console.log(isDivisibileBy4);

		  }
		

	checkDivisibilityBy4(56); 
	checkDivisibilityBy4(95); 
	checkDivisibilityBy4(444);

	//Functions as arguments
	//Function parameters can also accept other functions as arguments

	function argumentFunction(){
		console.log("This function was passed as an argument before the message was printed.");

	};

	function invokeFuntion(argumentFunction){
		argumentFunction();
	}

	//adding and removing the parenthesis "()" impacts the output of JS heavily
	//when a function is used with parenthesis "()", it denotes invoking/calling a function
	//a function used without a parenthesis is normally associated with using the functions as an argumetn to another function

	invokeFuntion(argumentFunction);

	//Using multiple parameters

	function createFullName(firstName, middleName, lastName){
		console.log(firstName + '' + middleName + '' + lastName);
	};


	createFullName('Juan','Dela','Cruz');
	createFullName('Cruz','Dela','Juan');
	createFullName('Juan','Dela');
	createFullName('Juan','','Cruz');
	createFullName('Juan','Dela','Cruz','III');

	//Using variable as arguments

	let firstName = "john"
	let middleName = "Doe"
	let lastName = "Smith"

	createFullName(firstName,middleName,lastName);

	//create a function called printfriends, 3parameters friend1, friend2, friend3

		function printFriends(friend1, friend2, friend3) {
	  console.log("My three friends are: " + friend1 + ", " + friend2 + ", " + friend3 + ".");
	}
	

	printFriends("amy", "lulu", "Morgan")

	//Return Statement

	function returnFullName(firstName, middleName, lastName) {
	  return firstName + ' ' + middleName + ' ' + lastName;
	}

	let completeName1 = returnFullName("Monkey", "D", "Luffy");
	let completeName2 = returnFullName("Cardo", "Tanggol", "Dalisay");

	console.log(completeName1 + " is my best friend.");
	console.log(completeName2 + " is my friend!");
/*
	Mini Activity

	1. Create a function that will calculate an area of a square 
	2. Create a function that will add 3 numbers 
	3. Create a function that will check if the number is equal to 100*/

	function calculateSquareArea(sidelenght){
		return sidelenght**2
	};

	let areaSquare = calculateSquareArea(4);


	function computeSum(num1,num2,num3){
		return(num1 + num2 + num3)
	}

	let sumOfThreeDigits = computeSum(1,2,3);
	console.log("the sum of three numbers are: ");


	function compareToOneHundred(num) {
	  return num === 100;
	}

	let booleanValue = compareToOneHundred(100);
	console.log("Is this one hundred?");
	console.log(booleanValue);
