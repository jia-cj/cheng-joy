console.log("Hi, B297");

//Mini-Activity - log your favorite movie lines 20 times in the console.

console.log("Sometimes the devil allows people to live a life free of trouble because he doesn't want them turning to God. Their sin is like a jail cell, except it is all nice and comfy and there doesn't seem to be any reason to leave - Gods Not Dead");

function printline (){
	console.log("Sometimes the devil allows people to live a life free of trouble because he doesn't want them turning to God. Their sin is like a jail cell, except it is all nice and comfy and there doesn't seem to be any reason to leave - Gods Not Dead");
};

printline();

//functions
//lines/blocks of code that tell our devices to perform a certain task when called/invoked

//Function Declaration

/*
	Syntax:

	function functionName() {
		code block (statement)
	}

*/

	//function declaration

	function printName () {
		console.log("My name is J.")
	};

	// function invocation
	printName();

	declaredFunction();

	//Function declaration vs expressions
	//Function Declaration
		//is created with the function keywordq adding a function name
	//they are "saved for the later"

	function declaredFunction(){
		console.log("Hello from declaredFunction!")
	}

	declaredFunction();

	//Function Expression 
		//is stored in a variable
		//expression is an anonymous function assigned to the variable function

	let variableFunction = function(){
		console.log("Hello from function expression")


	};

	variableFunction();

	//a function expression of function named funcName assigned to the variable funcExpression

	let funcExpression = function funcName() {
		console.log("Hello from the other side!");

	};

	funcExpression();

	// we can also reasssign declared functions and function expressions to new anonymous functions
	declaredFunction =  function() {
		console.log("updated declaredFunction");

	};

	declaredFunction();

	funcExpression = function() {
		console.log("updated funcExpression")
	};

	funcExpression();
 
 	const constantFunc = function(){
 		console.log("Initialized with const!")
 	};

 /*	constantFunc();

 //Function Scoping

 	Scope - acessibility/visibility of variables

 	JS Variables has 3 types of scope
 	1. local/block
 	2. global scope
 	3. Function Scope

 */

	 	{
	  let localVar = "Armando Perez";
	}

	{
	  let globalVar = "Mr. Worldwide";
	  console.log(globalVar);
	}

	function showNames() {
	  var functionVar = "Joe";
	  const functionConst = "John";
	  let functionLet = "Jane";

	  console.log(functionVar);
	  console.log(functionConst);
	  console.log(functionLet);
	}

	showNames();



 	//Nested Functions

 	function myNewFunction(){

 		let name = "Jane";

 		function nestedFunction(){
 			let nestedName = "John"
 			console.log(name);

 		}

 		nestedFunction();
 	}

 	myNewFunction();
 	// nestedFunction(); //is declared inside the myNewFunctions scope

 	//Function and Global Scoped Variables

 	let globalName = "Cardo";

 	function myNewFunction2(){
 		let nameInside = "Hillary"
 		console.log(globalName);
 	};

 	myNewFunction2();

 	//Using alert()

 	// alert("this will run immediately when the page loads")

 	function showSampleAlert (){
 		alert("Hello, Earthlings")
 	};

 	showSampleAlert();

 	console.log("I will only log in the console when the alert is dismissed!")

 	//Using Prompt()

 	let samplePrompt = prompt("Enter your Name:");

 	// console.log("Hi, I am" + samplePrompt)

 	//prompt returns an empty string when there is no input, or null if the user cancles the prompt()

	  function printWelcomeMessage() {
	  let firstName = prompt("Enter your first Name: ");
	  let lastName = prompt("Enter your last Name: ");

	  console.log("Hello, " + firstName + " " + lastName + "!");
	  console.log("Welcome to my page");
}

 	printWelcomeMessage();

 	//The Return Statement

 	// 


 	function returnFullName(){
 		return "Jeffrey" + ''+"Smith" + '' + "Bezos";
 		console.log("this message will not be printed!");
 	}

 	let fullName = returnFullName();
 	console.log(fullName);

	 	function returnFullAddress() {
	  let fullAddress = {
	    street: "#44 Maharlika St.",
	    city: "Cainta",
	    province: "Rizal"
	  }

	  return fullAddress;
	}
 	

 	let myAddress = returnFullAddress();
 	console.log(myAddress);

 	function printplayerInfo(){

 		console.log("Username:" + "dark_magician")
 		console.log("Level:" + 95);
 		console.log("Job: " + "Mage");

 	}

 	let user1 = printplayerInfo()
 	console.log(user1); //indefined

	 function returnSumOf5and10() {
	  return 5 + 10;
	}

	let sumOf5and10 = returnSumOf5and10();
	console.log(sumOf5and10);

	let total = 100 + sumOf5and10();
	console.log(total);

	function getGuildMembers(){
		return ["lulu", "Tristana", "Teemo"];
	}
	console.log(getGuildMembers());

	// Functions Naming Convections

	function getCourses(){
		let courses = ["reactjs", "expressjs 101", "MongoDB 101"];
		return courses;	
	}

//avoid using generic names and pointless inapropriate function names
	function pikachu(){
		let color = "pink"
		return name;
	}

	function displayCarInfo(){
		console.log("Brand: Toyota");
		console.log("Type: Sedan");

	}

	displayCarInfo();



	//

	function getUserInfo(){
		return{
			 name: "John Doe",
			 age: 25,
			 address: "123 Qc",
			 isMarried: false,
			 petName: "jin"		}
	}

	let userInfo = getUserInfo();
	console.log(userInfo);