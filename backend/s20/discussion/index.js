console.log("Hello World");

// Arithmetic Operators
// + - * / %

let x = 1397;
let y = 7831;

let sum = x + y;
console.log("Result of addition Operator: " + sum)

let difference = x - y;
console.log("Result of subtraction Operator: " + difference)

let product = x * y;
console.log("Result of multiplication Operator: " + product)

let quotient = x / y;
console.log("Result of division Operator: " + quotient)

let remainder = y % x;
console.log("Result of modulo Operator: " + remainder)

let a = 10;
let b = 5;

let remainderA = a % b;
console.log(remainderA);

// Assignment Operator
// Basic Assignment Operator (=)

let assignmentNumber = 8;

// Addition assignment operator (+=)

assignmentNumber = assignmentNumber + 2;
assignmentNumber += 2;
console.log(assignmentNumber);

//Subtraction/Multiplication/Division Assignment operator

assignmentNumber -= 2;
console.log("Result of -= : " + assignmentNumber)

assignmentNumber *= 2;
console.log("Result of -= : " + assignmentNumber)

assignmentNumber /= 2;
console.log("Result of -= : " + assignmentNumber)

//Multiple Operators and Parenthesis

/*
	1. 3 * 4 = 12
	2. 12 / 5 = 2.4
	3. 1 + 2 = 3
	4. 3 - 2.4 = 0.6

*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

let pemdas = 1 + (2-3) * (4/5);
console.log(pemdas);

//Increment and Decrement
//Operators that add or Subtract values by 1 and reassigns the value of the variable where the increment/decrement was applied to

let z = 1;

let increment = ++z;

//the value of z is added by a value of one before returning the value and storing it in the variable "increment"

console.log("Result of pre-increment: " + increment);
console.log("Result of pre-increment: " +z);

//the value of z is returned and stored in the variable "increment" then the value of z in increased by one

increment = z++;
console.log("Result of post-increment: " + increment);
console.log("Result of post-increment: " +z);


//decrease and reassign
let decrement = --z;

//the value of z is added by a value of one before returning the value and storing it in the variable "increment"

console.log("Result of pre-decrement: " + decrement);
console.log("Result of pre-decrement: " +z);

//reassign the decrease
decrement = z--;
console.log("Result of post-decrement: " + decrement);
console.log("Result of post-decrement: " +z);

//Type Coercion

//implicit value of numbers

let numA = 10;
let numB = 12;

let coercion = numA + numB;
console.log(coercion);//"1012"

let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion)
console.log(typeof nonCoercion);

// the boolean true is assoc with value = 1

let numE = true + 1;
console.log(numE);

//the boolean false is assoc with the value of 0

let numF = false + 1;
console.log(numF);

// Comparison operators 

let juan = 'juan';

//Equality operators (==)


console.log(1 == 1); //true
console.log(1 == 2); //false
console.log(1 == '1'); //true
console.log(0 == false); //true
console.log('juan' == 'juan'); //true
console.log('juan' == juan); //true

//Inequality operators (!=)

console.log(1 != 1); //false
console.log(1 != 2); //true
console.log(1 != '1'); //false
console.log(0 != false); //false
console.log('juan' != 'juan'); //false
console.log('juan' != juan); //false

// Strict Equality operators (===)


console.log(1 === 1); //true
console.log(1 === 2); //false
console.log(1 === '1'); //false
console.log(0 === false); //true
console.log('juan' === 'juan'); //true
console.log('juan' === juan); //true

//Strict Inequality operators (!=)

console.log(1 !== 1); //false
console.log(1 !== 2); //true
console.log(1 !== '1'); //true
console.log(0 !== false); //true
console.log('juan' !== 'juan'); //false
console.log('juan' !== juan); //false

//relational Operators

let abc = 50;
let def = 65;

let isGreaterThan = abc > def
let isLessThan = abc < def
let isGTOrThan = abc >= def
let isLTOrThan = abc <= def

console.log(isGreaterThan)
console.log(isLessThan)
console.log(isGTOrThan)
console.log(isLTOrThan)

let numStr = '30';
console.log(abc > numStr);
console.log(abc <= numStr);

let str = "twenty";
console.log(def >= str);


// Logical Operators

let isLegalAge = true
let isRegistered = false

//&& (AND), || (OR), !(NOT)


//&&
//Return true if all operands are true

let allRequirementsMet = isLegalAge && isRegistered;
console.log("Result of AND Operator: " + allRequirementsMet);

// ||
//Returns true if one operands are true

let someRequirementsMet = isLegalAge || isRegistered;
console.log("Result of AND Operator: " + someRequirementsMet);

//!
//Returns the opposite value
let someRequirementsNotMet = isLegalAge =! isRegistered;
console.log("Result of AND Operator: " + someRequirementsMet);